import os
import time

import requests
from flask import request, Flask
import mysql.connector
from mysql.connector import errorcode
from xml.etree import ElementTree as ET
import urllib.parse as urlparse
from urllib.parse import parse_qs
import json 
import re


app = Flask(__name__)

source = "https://raw.githubusercontent.com/devdattakulkarni/elements-of-web-programming/master/data/austin-pool-timings.xml"

data = requests.get(source)

root = ET.fromstring(data.text)

# get database credentials to connect 
def get_db_creds():
  db = os.environ.get("DB", None) or os.environ.get("database", None)
  username = os.environ.get("USER", None) or os.environ.get("username", None)
  password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
  hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
  return db, username, password, hostname

def create_table():
  # Check if table exists or not. Create and populate it only if it does not exist.
  db, username, password, hostname = get_db_creds()
  table_ddl = 'CREATE TABLE pools(pool_name varchar(100) NOT NULL, status varchar(100) NOT NULL, phone varchar(100) NOT NULL, pool_type varchar(100) NOT NULL,  PRIMARY KEY (pool_name))'
  cnx = ''
  try:
    cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
  except Exception as exp:
    print(exp)
    import MySQLdb
    # try:
    cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
  # except Exception as exp1:
  # print(exp1)
  cur = cnx.cursor()
  try:
    cur.execute(table_ddl)
    cnx.commit()
    #populate_data()
  except mysql.connector.Error as err:
    if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
      print()
    else:
      print(err.msg)

# populate the table with city of austin pool data
def populate_table():
  db, username, password, hostname = get_db_creds()
  cnx = ''
  try:
    cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
  except Exception as exp:
    print(exp)
    import MySQLdb
    cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
  for pool in root.findall("row"):
    in_pool_name = pool.find("pool_name").text
    in_pool_status = pool.find("status").text
    in_pool_phone = pool.find("phone").text
    in_pool_type = pool.find("pool_type").text
    cur = cnx.cursor()
    cur.execute("INSERT IGNORE INTO pools (pool_name, status, phone, pool_type) values ('"+in_pool_name+"', '"+in_pool_status+"', '"+in_pool_phone+"', '"+in_pool_type+"')")
    cnx.commit()

def read_curl_file(file):
  my_file = request.json
  return str(my_file)
  # def print_filename():
  #   file = request.files['file']
  #   filename=secure_filename(file.filename)   
  #   return filename

@app.route('/')
def output():
  create_table()
  populate_table()
  return "Welcome to City of Austin Pools Database"

@app.route('/pools', methods = ['GET', 'POST'])
def output_pools():
  create_table()
  populate_table()

  # POST a new pool into datbase
  if request.method == "POST":
    # connect to datbase
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
      cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
      print(exp)
      import MySQLdb
      cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    # now read input data from JSON file

    valid_values_status = ["Closed", "Open", "In Renovation"]
    valid_values_pool_types = ["Neighborhood", "University", "Community"]

    my_data = request.json

    in_pool_name = my_data["pool_name"]
    in_pool_status = my_data["status"]
    in_pool_phone = my_data["phone"]
    in_pool_type = my_data["pool_type"]
    cur = cnx.cursor(buffered=True)
    cur.execute("SELECT EXISTS(SELECT * FROM pools WHERE pool_name = '%s')" % (in_pool_name))
    cnx.commit()
    row = cur.fetchall()
    # return str(exists) + "\n" + str(in_pool_name)
    if row[0][0] == 1:
      return "Status: 400 Bad Data" + "\n" + "Message: Pool with name '%s' already exists" % (in_pool_name)
    elif in_pool_status not in valid_values_status:
      return "Status: 400 Bad Data" + "\n" + "Message: status field has invalid value"
    elif in_pool_type not in valid_values_pool_types:
      return "Status: 400 Bad Data" + "\n" + "Message: pool_type field has invalid value"
    elif not re.fullmatch(r"^\d{3}-\d{3}-\d{4}$", in_pool_phone):
      return "Status: 400 Bad Data" + "\n" + "Message: phone field not in valid format"
    elif row[0][0] == 0:
      if in_pool_status in valid_values_status:
        if in_pool_type in valid_values_pool_types:
          if re.fullmatch(r"^\d{3}-\d{3}-\d{4}$", in_pool_phone):
            cur = cnx.cursor()
            cur.execute("INSERT IGNORE INTO pools (pool_name, status, phone, pool_type) values ('"+in_pool_name+"', '"+in_pool_status+"', '"+in_pool_phone+"', '"+in_pool_type+"')")
            cnx.commit()
            return "Status: 201 created"

@app.route('/pools/<wanted_pool_name>', methods = ['GET', 'PUT', 'DELETE'])
def output_wanted_pool_name(wanted_pool_name):
  create_table()
  populate_table()

  # get info about specific pool 
  if request.method == "GET":
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
      cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
      print(exp)
      import MySQLdb
      cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    cur.execute("SELECT EXISTS(SELECT * FROM pools WHERE pool_name = '%s')" % (wanted_pool_name))
    cnx.commit()
    row = cur.fetchall()
    if row[0][0] == 0:
      return "Status: 404 Not Found" + "\n" + "Message: Pool with name '%s' does not exist" % (wanted_pool_name)
    elif row[0][0] == 1:
      cur = cnx.cursor()
      cur.execute("SELECT * from pools where pool_name = '%s'" % (wanted_pool_name))

      row = cur.fetchall()
      wanted_dict = {}
      wanted_dict["pool_name"] = row[0][0]
      wanted_dict["status"] = row[0][1]
      wanted_dict["phone"] = row[0][2]
      wanted_dict["pool_type"] = row[0][3]

      return "Status: 200 OK" + "\n" + json.dumps(wanted_dict) 

  # update a pool into database
  elif request.method == "PUT":

    # connect to datbase
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
      cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
      print(exp)
      import MySQLdb
      cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    # now read input data from JSON file 
    my_data = request.json

    valid_values_status = ["Closed", "Open", "In Renovation"]
    valid_values_pool_types = ["Neighborhood", "University", "Community"]

    in_pool_name = my_data["pool_name"]
    in_pool_status = my_data["status"]
    in_pool_phone = my_data["phone"]
    in_pool_type = my_data["pool_type"]

    dict_update = {}
    dict_update["pool_name"] = in_pool_name
    dict_update["status"] = in_pool_status
    dict_update["phone"] = in_pool_phone
    dict_update["pool_type"] = in_pool_type

    if in_pool_status not in valid_values_status:
      return "Status: 400 Bad Data" + "\n" + "Message: status field has invalid value"
    elif in_pool_type not in valid_values_pool_types:
      return "Status: 400 Bad Data" + "\n" + "Message: pool_type field has invalid value"
    elif not re.fullmatch(r"^\d{3}-\d{3}-\d{4}$", in_pool_phone):
      return "Status: 400 Bad Data" + "\n" + "Message: phone field not in valid format"

    elif wanted_pool_name != in_pool_name:
      return "Status: 400 Bad Data" + "\n" + "Message: Update to pool_name field not allowed"
    cur = cnx.cursor(buffered=True)
    cur.execute("SELECT EXISTS(SELECT * FROM pools WHERE pool_name = '%s')" % (wanted_pool_name))
    cnx.commit()
    row = cur.fetchall()
    if row[0][0] == 0:
      return "Status: 404 Not Found" + "\n" + "Message: Pool with name '%s' does not exist" % (wanted_pool_name)
    elif row[0][0] == 1:
      # verify and print out output
      cur = cnx.cursor()
      cur.execute("DELETE from pools where pool_name = '%s'" % (in_pool_name))
      cnx.commit()
      cur = cnx.cursor()
      cur.execute("INSERT IGNORE INTO pools (pool_name, status, phone, pool_type) values ('"+in_pool_name+"', '"+in_pool_status+"', '"+in_pool_phone+"', '"+in_pool_type+"')")
      cnx.commit()

      #verify
      return "Status: 200 OK" + "\n" + json.dumps(dict_update)

  # delete a pool from the database
  elif request.method == "DELETE":
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
      cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
      print(exp)
      import MySQLdb
      cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    cur = cnx.cursor(buffered=True)
    cur.execute("SELECT EXISTS(SELECT * FROM pools WHERE pool_name = '%s')" % (wanted_pool_name))
    cnx.commit()
    row_delete = cur.fetchall()
    if row_delete[0][0] == 0:
      return "Status: 404 Not Found" + "\n" + "Message: Pool with name '%s' does not exist" % (wanted_pool_name)
    elif row_delete[0][0] == 1:
      cur = cnx.cursor()
      cur.execute("DELETE from pools where pool_name = '%s'" % (wanted_pool_name))
      cnx.commit()
      return "Status: 200 OK"

# Starts the web server
if __name__ == "__main__":
    create_schema()
    app.run()